// noinspection JSUnresolvedFunction,JSUnresolvedVariable

const jsonSchema = {
    type: "object",
    properties: {
        _id: {
            bsonType: "objectId",
        },
        id: {
            bsonType: "int",
            minimum: 10000,
            maximum: 20000,
        },
        name: {
            type: "string",
            minLength: 1
        },
        username: {
            type: "string",
            minLength: 1
        },
        quotaInGB: {
            bsonType: "int",
            minimum: 1
        },
        aliases: {
            type: "array",
            items: {
                type: "string",
                minLength: 1
            },
        },
        php: {
            type: "object",
            properties: {
                version: {
                    type: "string",
                    pattern: "^\\d\\.\\d$",
                },
                request_timeout: {
                    bsonType: "int",
                    minimum: 1
                },
                pm: {
                    type: "object",
                    properties: {
                        max_children: {
                            bsonType: "int",
                        },
                    },
                    required: ["max_children"],
                    additionalProperties: false,
                },
                memory_limit: {
                    type: "string",
                    pattern: "^\\d+[KMG]?$"
                },
                env: {
                    type: "object",
                    patternProperties: {
                        "^\\w+$": {type: "string"},
                    },
                },
                enable_all_versions: {
                    type: "boolean"
                }
            },
            required: ["version", "request_timeout", "pm", "memory_limit"],
            additionalProperties: false,
        },
        sftp: {
            type: "object",
            properties: {
                keys: {
                    type: "array",
                    items: {
                        type: "string",
                        minLength: 1
                    },
                },
                password: {
                    type: "string",
                    minLength: 1
                },
            },
            additionalProperties: false,
        },
    },
    required: ["_id", "id", "name", "username", "php"],
    additionalProperties: false,
};

db = new Mongo().getDB("db");

db.createCollection(
    "sites",
    {validator: {$jsonSchema: jsonSchema}}
);

db.sites.createIndex({ id: 1 }, {unique: true});
db.sites.createIndex({ name: 1 }, {unique: true});
db.sites.createIndex({ username: 1 }, {unique: true});
