// noinspection JSUnresolvedFunction

db = new Mongo().getDB("db");

db.createUser({
    user: 'user',
    pwd: 'password',
    roles: [{role: 'read', db: 'db'}]
});