import {KubeConfig} from "@kubernetes/client-node";
import {Logger} from "./Logger";
import {InfiniteK8sWatch} from "./InfiniteK8sWatch";
import {singleton} from "tsyringe";

@singleton()
export class InfiniteK8sWatchFactory {
    public constructor(
        private readonly kc: KubeConfig,
        private readonly logger: Logger,
    ) {
    }

    public build(): InfiniteK8sWatch {
        return new InfiniteK8sWatch(this.kc, this.logger);
    }
}
