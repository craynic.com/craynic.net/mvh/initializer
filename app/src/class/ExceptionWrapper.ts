export class ExceptionWrapper extends Error {
    constructor(readonly message: string, public readonly wrappedException: any) {
        super(message);
    }
}
