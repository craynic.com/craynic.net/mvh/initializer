import {inject, singleton} from "tsyringe";
import {ArgvType} from "../../schema/argvSchema";
import {SiteConfig} from "../../schema/configSchema";
import {FSFilename} from "./FSHelper";
import * as crypto from "crypto";

export enum PathType {
    MOUNTED = 1,
    TARGET = 2,
    MOUNTED_ARCHIVED = 3,
}

export enum Dir {
    ROOT = '',
    HOME = 'home',
    ETC = 'etc',
    HTDOCS = 'htdocs',
    LOGS = 'logs',
    LOGS_APACHE = `logs/apache`,
    LOGS_CRON = 'logs/cron',
    LOGS_PHP = 'logs/php',
    TMP = 'tmp',
    TMP_PHP = 'tmp/php',
    TMP_UPLOAD = 'tmp/upload',
}

@singleton()
export class HomeDirHelper {
    public constructor(
        @inject('cli-args') private readonly cliArgs: ArgvType,
    ) {
    }

    public getPath(siteConfig: SiteConfig, pathType: PathType, dir: Dir): FSFilename {
        let path: FSFilename = '';

        if (pathType === PathType.MOUNTED) {
            path += `${this.cliArgs["mounted-sites-root-dir"]}/`;
        } else if (pathType === PathType.TARGET) {
            path += `${this.cliArgs["target-sites-root-dir"]}/`;
        } else if (pathType === PathType.MOUNTED_ARCHIVED) {
            path += `${this.cliArgs["mounted-sites-root-dir"]}/_archived_${Date.now()}_${crypto.randomUUID()}_`;
        } else {
            throw 'Unknown path type';
        }

        path += `${siteConfig.name}${(dir !== '') ? `/${dir}` : ``}`;

        return path;
    }
}
