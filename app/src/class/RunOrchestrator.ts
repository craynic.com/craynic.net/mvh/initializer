import {HomeDirBuilder} from "./ConfigProcessor/HomeDirBuilder";
import {promiseAllResolved} from "../function/promiseAllResolved";
import {ApacheConfigGenerator} from "./ConfigProcessor/ApacheConfigGenerator";
import {PhpConfigGenerator} from "./ConfigProcessor/PhpConfigGenerator";
import {SFTPKeysWriter} from "./ConfigProcessor/SFTPKeysWriter";
import {UsersWriter} from "./ConfigProcessor/UsersWriter";
import {SiteConfig} from "../schema/configSchema";
import {singleton} from "tsyringe";
import {SitesDb} from "./SitesDb";
import {Logger} from "./Logger";
import {ExceptionWrapper} from "./ExceptionWrapper";

type PendingChangeAdd = { change: "added", added: SiteConfig };
type PendingChangeUpdate = { change: "updated", from: SiteConfig, to: SiteConfig }
type PendingChangeRemove = { change: "removed", removed: SiteConfig }
type PendingChange = PendingChangeAdd | PendingChangeUpdate | PendingChangeRemove;

@singleton()
export class RunOrchestrator {
    private pendingChanges: Array<PendingChange> = [];

    public constructor(
        private readonly sitesDb: SitesDb,
        private readonly logger: Logger,
        private readonly homeDirBuilder: HomeDirBuilder,
        private readonly apacheConfigGenerator: ApacheConfigGenerator,
        private readonly phpConfigGenerator: PhpConfigGenerator,
        private readonly sftpKeysWriter: SFTPKeysWriter,
        private readonly usersWriter: UsersWriter
    ) {
        this.sitesDb
            .on("site_added", (siteConfig: SiteConfig) => {
                this.recordPendingChange({change: "added", added: siteConfig});
            })
            .on("site_updated", (oldSiteConfig: SiteConfig, newSiteConfig: SiteConfig) => {
                this.recordPendingChange({change: "updated", from: oldSiteConfig, to: newSiteConfig});
            })
            .on("site_removed", (siteConfig: SiteConfig) => {
                this.recordPendingChange({change: "removed", removed: siteConfig});
            });
    }

    private recordPendingChange(pendingChange: PendingChange): void {
        this.pendingChanges.push(pendingChange);
        this.pendingChangesPromiseResolve();
    }

    private pendingChangesPromiseResolve(): void {
    }

    public async processPendingChanges(): Promise<void> {
        const pendingChanges: PendingChange[] = [...this.pendingChanges.values()],
            restartPromise: Promise<void> = (new Promise<void>(resolve => {
                this.pendingChangesPromiseResolve = resolve;
            }));

        this.pendingChanges = [];

        return pendingChanges.reduce(
            async (previousValue: Promise<void>, currentValue: PendingChange): Promise<void> => {
                if (currentValue.change === "added") {
                    await previousValue;
                    return await this.onSiteAdded(currentValue.added);
                }

                if (currentValue.change === "updated") {
                    await previousValue;
                    return await this.onSiteUpdated(currentValue.from, currentValue.to);
                }

                if (currentValue.change === "removed") {
                    await previousValue;
                    return await this.onSiteRemoved(currentValue.removed);
                }

                throw 'Unknown type of a pending change';
            },
            Promise.resolve()
                .then((): void => {
                    if (pendingChanges.length > 0) {
                        this.logger.debug(pendingChanges.length === 1
                            ? `Processing 1 pending change...`
                            : `Processing ${pendingChanges.length} pending changes...`
                        );
                    }
                })
        )
        .then(async (): Promise<void> => {
            this.logger.debug('Waiting for pending changes...');

            return restartPromise
                .then(async (): Promise<void> => this.processPendingChanges())
        });
    }

    public async runFull(siteConfigs: SiteConfig[]): Promise<void> {
        this.logger.debug(`Starting full run`);

        return this.homeDirBuilder.runFull(siteConfigs).then(() =>
            promiseAllResolved<void>([
                this.apacheConfigGenerator.runFull(siteConfigs),
                this.phpConfigGenerator.runFull(siteConfigs),
                this.sftpKeysWriter.runFull(siteConfigs),
                this.usersWriter.runFull(siteConfigs),
            ])
            .then((): void => {})
        )
        .then((): void => {
            this.logger.info('Full run finished successfully');
        })
        .catch((e): never => {
            throw new ExceptionWrapper('Full run finished with errors:', e);
        });
    }

    public async onSiteAdded(siteConfig: SiteConfig): Promise<void> {
        this.logger.info(`Adding site ${siteConfig.name} ...`);

        return this.homeDirBuilder.onSiteAdded(siteConfig).then(() =>
            promiseAllResolved<void>([
                    this.phpConfigGenerator.onSiteAdded(siteConfig)
                        .then(async (): Promise<void> => this.apacheConfigGenerator.onSiteAdded(siteConfig)),
                    this.sftpKeysWriter.onSiteAdded(siteConfig),
                    this.usersWriter.onSiteAdded(siteConfig),
                ])
                .then((): void => {
                    this.logger.info(`Site ${siteConfig.name} added.`);
                })
        );
    }

    public async onSiteUpdated(oldSiteConfig: SiteConfig, newSiteConfig: SiteConfig): Promise<void> {
        if (oldSiteConfig.id !== newSiteConfig.id) {
            throw `Site ID must be immutable! (Old site name ${oldSiteConfig.name} with ID ${oldSiteConfig.id}`
                + ` changed to ID ${newSiteConfig.id}.`;
        }

        this.logger.info(`Updating site ${oldSiteConfig.name}...`);

        return Promise.resolve()
            .then(async (): Promise<void> => {
                return oldSiteConfig.name !== newSiteConfig.name
                    ? this.onSiteUpdatedWithNameChange(oldSiteConfig, newSiteConfig)
                    : this.onSiteUpdatedSimple(oldSiteConfig, newSiteConfig);
            })
            .then((): void => {
                this.logger.info(`Site ${oldSiteConfig.name} updated.`);
            });
    }

    private async onSiteUpdatedWithNameChange(oldSiteConfig: SiteConfig, newSiteConfig: SiteConfig): Promise<void> {
        this.logger.info(
            `Site ${oldSiteConfig.name} name changed to ${newSiteConfig.name}, updating through disable/re-enable...`
        );

        return promiseAllResolved<void>([
                this.apacheConfigGenerator.onSiteRemoved(oldSiteConfig),
                this.phpConfigGenerator.onSiteRemoved(oldSiteConfig),
                this.sftpKeysWriter.onSiteRemoved(oldSiteConfig),
                this.usersWriter.onSiteRemoved(oldSiteConfig),
            ])
            .then(async (): Promise<void> =>
                this.homeDirBuilder.onSiteUpdated(oldSiteConfig, newSiteConfig)
            )
            .then(async (): Promise<void[]> => promiseAllResolved<void>([
                this.phpConfigGenerator.onSiteAdded(newSiteConfig)
                    .then(async (): Promise<void> =>
                        this.apacheConfigGenerator.onSiteAdded(newSiteConfig)
                    ),
                this.sftpKeysWriter.onSiteAdded(newSiteConfig),
                this.usersWriter.onSiteAdded(newSiteConfig),
            ]))
            .then((): void => {});
    }

    private async onSiteUpdatedSimple(oldSiteConfig: SiteConfig, newSiteConfig: SiteConfig): Promise<void> {
        return this.homeDirBuilder.onSiteUpdated(oldSiteConfig, newSiteConfig)
            // let's update PHP first... in case the PHP version changes,
            // PhpConfigGenerator performs graceful update
            .then(async (): Promise<void> =>
                this.phpConfigGenerator.onSiteUpdated(oldSiteConfig, newSiteConfig))
            .then(async (): Promise<void[]> => promiseAllResolved<void>([
                this.apacheConfigGenerator.onSiteUpdated(oldSiteConfig, newSiteConfig),
                this.sftpKeysWriter.onSiteUpdated(oldSiteConfig, newSiteConfig),
                this.usersWriter.onSiteUpdated(oldSiteConfig, newSiteConfig),
            ]))
            .then((): void => {});
    }

    public async onSiteRemoved(siteConfig: SiteConfig): Promise<void> {
        this.logger.info(`Removing site ${siteConfig.name}...`);

        return promiseAllResolved<void>([
                this.apacheConfigGenerator.onSiteRemoved(siteConfig),
                this.phpConfigGenerator.onSiteRemoved(siteConfig),
                this.sftpKeysWriter.onSiteRemoved(siteConfig),
                this.usersWriter.onSiteRemoved(siteConfig),
            ])
            .then(async (): Promise<void> =>
                this.homeDirBuilder.onSiteRemoved(siteConfig)
            )
            .then((): void => {
                this.logger.info(`Site ${siteConfig.name} removed.`);
            });
    }
}
