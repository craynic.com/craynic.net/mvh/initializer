import "reflect-metadata";
import {inject, singleton} from "tsyringe";
import {Logger} from "./Logger";
import * as k8s from "@kubernetes/client-node";
import {
    Context,
    KubeConfig,
    KubernetesObject,
    KubernetesObjectApi,
    PatchStrategy,
    V1ListMeta,
    V1ObjectMeta,
    V1Secret,
    V1SecretList,
    VersionInfo
} from "@kubernetes/client-node";
import {IncomingMessage} from "http";
import {promiseAllResolved} from "../function/promiseAllResolved";
import {ArgvType} from "../schema/argvSchema";
import {FSFileContents, FSFileList, FSFilename, FSFilenameList, FSHelper} from "./Helper/FSHelper";
import {Settings} from "../Settings";
import {InfiniteK8sWatchFactory} from "./InfiniteK8sWatchFactory";
import {InfiniteK8sWatch} from "./InfiniteK8sWatch";
import {SiteConfig} from "../schema/configSchema";
import crypto from "crypto";
import {promiseDelay} from "../function/promiseDelay";
import {ExceptionWrapper} from "./ExceptionWrapper";

declare type V1CertificateIssuerKind = 'Issuer' | 'ClusterIssuer';

declare type V1CertificateIssuerRef = {
    kind: V1CertificateIssuerKind,
    name: string,
}

declare type V1Certificate = {
    apiVersion: 'cert-manager.io/v1',
    kind: 'Certificate',
    metadata: V1ObjectMeta & {
        name: string,
        'labels': {
            [key: string]: string,
        },
    },
    spec: {
        dnsNames: string[],
        secretName: string,
        issuerRef: V1CertificateIssuerRef,
    }
};

declare type V1CertificateList = {
    kind: 'CertificateList',
    apiVersion: 'cert-manager.io/v1',
    metadata: V1ListMeta,
    items: V1Certificate[]
};

export type TLSCertificate = {
    publicKeyFilename: string,
    privateKeyFilename: string,
}

export type TLSCertificateForSite = {
    tlsCertificate: TLSCertificate,
    hostNames: string[]
}

export type TLSCertificatesForSites = Map<SiteId, TLSCertificateForSite[]>;

type SiteId = number;
type CertificateName = string;
type CertificateMap = Map<CertificateName, V1Certificate>
type SiteCertificatesMap = Map<SiteId, CertificateMap>;

type TLSSecretData = {
    'tls.crt': string,
    'tls.key': string,
    'ca.crt'?: string,
};

type FullyLoadedTLSSecret = V1Secret & {
    metadata: V1ObjectMeta & {
        name: string,
        resourceVersion: string,
    },
    data: TLSSecretData & {
        [key: string]: string,
    },
}

@singleton()
export class K8sCertificateManager {
    private static readonly TLS_SECRET_FIELD_SELECTOR: string = 'type=kubernetes.io/tls';
    private static readonly SITE_ID_LABEL_KEY: string = 'craynic.net/site-id';
    private static readonly MAXIMUM_HOSTNAMES_PER_CERTIFICATE: number = 100;
    private static readonly ADDITIONAL_CERTIFICATE_LABELS: { [key: string]: string } = {
        'craynic.net/component': 'certificate'
    };
    private static readonly K8s_MAXIMUM_CONNECTION_ATTEMPTS: number = 5;

    private _targetNamespace: string|undefined;
    private get targetNamespace(): string {
        if (this._targetNamespace === undefined) {
            this._targetNamespace = this.cliArgs["target-namespace"] !== ''
                ? this.cliArgs["target-namespace"]
                : this.detectCurrentNamespace();
        }

        return this._targetNamespace;
    }

    private readonly k8sCoreApi: k8s.CoreV1Api;
    private readonly k8sCustomObjectsApi: k8s.CustomObjectsApi;
    private readonly k8sObjectApi: KubernetesObjectApi;

    private readonly siteCertificatesMap: SiteCertificatesMap = new Map<SiteId, CertificateMap>();

    private watches: InfiniteK8sWatch[] = [];

    constructor(
        private readonly logger: Logger,
        private readonly fsHelper: FSHelper,
        private readonly kc: KubeConfig,
        private readonly infiniteK8sWatchFactory: InfiniteK8sWatchFactory,
        @inject('cli-args') private readonly cliArgs: ArgvType,
    ) {
        this.k8sCoreApi = this.kc.makeApiClient(k8s.CoreV1Api);
        this.k8sCustomObjectsApi = this.kc.makeApiClient(k8s.CustomObjectsApi);
        this.k8sObjectApi = this.kc.makeApiClient(k8s.KubernetesObjectApi);
    }

    public async setUp(): Promise<void> {
        await this.connectToK8s();

        const resourceVersionCerts: string = await this.loadCertificates(),
            resourceVersionSecrets: string = await this.loadSecretsForCertificates(resourceVersionCerts);

        this.watches = await promiseAllResolved<InfiniteK8sWatch>([
            this.setWatchForCertificates(resourceVersionSecrets),
            this.setWatchForSecrets(resourceVersionSecrets),
        ]);

        this.logger.info('K8s certificate manager initialized.');
    }

    private async connectToK8s(attempt: number = 1): Promise<void> {
        return this.kc.makeApiClient(k8s.VersionApi)
            .getCode()
            .then((versionInfo: VersionInfo) => {
                this.logger.debug(
                    `Successfully connected to the K8s cluster version ${versionInfo.gitVersion} (attempt #${attempt}).`
                );
            })
            .catch(async (e: any): Promise<void> => {
                if (attempt >= K8sCertificateManager.K8s_MAXIMUM_CONNECTION_ATTEMPTS) {
                    throw new ExceptionWrapper(
                        'Could not connect to the K8s cluster, maximum attempts count reached.',
                        e
                    );
                }

                const waitDelay: number = 2 ** Math.min(attempt, 5);

                this.logger.warn(
                    `Error connecting to the K8s cluster in attempt #${attempt},`
                    + ` waiting ${waitDelay}s before retrying...`
                );

                await promiseDelay(waitDelay);
                await this.connectToK8s(attempt + 1);
            });
    }

    public async tearDown(): Promise<void> {
        return promiseAllResolved<void>(
            this.watches.map(
                async (watch: InfiniteK8sWatch): Promise<void> => {
                    this.logger.debug(`Destroying watch request...`);

                    return watch.abort();
                }
            )
        )
        .then((): void => {});
    }

    public async getCertificatesForAllSites(siteConfig: SiteConfig[]): Promise<TLSCertificatesForSites> {
        return promiseAllResolved<[SiteId, TLSCertificateForSite[]]>(
            siteConfig.map(
                async (siteConfig: SiteConfig): Promise<[SiteId, TLSCertificateForSite[]]> =>
                    this.getCertificatesForSite(siteConfig)
                        .then((certificatesForSite: TLSCertificateForSite[]): [SiteId, TLSCertificateForSite[]] =>
                            [siteConfig.id, certificatesForSite]
                        )
            )
        )
        .then(async (values: [SiteId, TLSCertificateForSite[]][]): Promise<TLSCertificatesForSites> => {
            const resultMap: TLSCertificatesForSites = new Map(values);

            return promiseAllResolved<void[]>(
                [...this.siteCertificatesMap.entries()]
                    .filter(([siteId]: [SiteId, CertificateMap]): boolean => !resultMap.has(siteId))
                    .map(async ([siteId, certificateMap]: [SiteId, CertificateMap]): Promise<void[]> => {
                        this.logger.info(`Removing all certificates for non-existent site id ${siteId}`);

                        return promiseAllResolved<void>(
                            [...certificateMap.values()]
                                .map(async (certificate: V1Certificate): Promise<void> =>
                                    this.k8sObjectApi.delete(certificate).then((): void => {
                                        this.logger.info(
                                            `Removed certificate ${certificate.metadata.name}`
                                            + ` for non-existent site id ${siteId}`);
                                    })
                                )
                        )
                    })
            )
            .then((): TLSCertificatesForSites => resultMap);
        });
    }

    public async getCertificatesForSite(siteConfig: SiteConfig): Promise<TLSCertificateForSite[]> {
        const hostNamesInSite: Set<string> = new Set<string>([siteConfig.name, ...(siteConfig.aliases ?? [])]),
            currentCertificatesSorted: Set<V1Certificate> = new Set<V1Certificate>(
                [...(this.siteCertificatesMap.get(siteConfig.id) ?? []).values()]
                    .sort((certificateA: V1Certificate, certificateB: V1Certificate): number =>
                        certificateB.spec.dnsNames.length - certificateA.spec.dnsNames.length
                    )
            ),
            hostNamesInAllCertificates: Set<string> = new Set<string>(
                [...currentCertificatesSorted.values()].reduce<string[]>(
                    (allHostNames: string[], certificate: V1Certificate): string[] =>
                        [...allHostNames, ...certificate.spec.dnsNames],
                    []
                )),
            hostNamesMissingInCertificates: Set<string> = new Set<string>(
                [...hostNamesInSite].filter((hostName: string): boolean => !hostNamesInAllCertificates.has(hostName))
            ),
            hostNamesRedundantInCertificates: Set<string> = new Set<string>(
                [...hostNamesInAllCertificates].filter((hostName: string): boolean => !hostNamesInSite.has(hostName))
            );

        this.logger.debug(
            `All host names in site ID ${siteConfig.id}: ${[...hostNamesInSite.values()].join(', ')}`
        );
        this.logger.debug(
            `All host names in certificates for site ID ${siteConfig.id}: ${[...hostNamesInAllCertificates.values()].join(', ')}`
        );

        // remove redundant host names from certificates
        if (hostNamesRedundantInCertificates.size > 0) {
            this.logger.debug(`Redundant host names in certificates for site ID ${siteConfig.id}: `
                + [...hostNamesRedundantInCertificates].join(', ')
            );

            this.removeHostNamesFromCertificates(currentCertificatesSorted, hostNamesRedundantInCertificates);
        }

        this.cleanupDuplicatedHostNames(currentCertificatesSorted);

        // add missing host names
        const newCertificates: Set<V1Certificate> = new Set();

        if (hostNamesMissingInCertificates.size > 0) {
            this.logger.debug(`Missing host names in certificates for site ID ${siteConfig.id}: `
                + [...hostNamesMissingInCertificates].join(', '));

            this.createNewCertificatesForHostNames(
                siteConfig.id,
                this.fillCertificateSetWithHostNames(currentCertificatesSorted, [...hostNamesMissingInCertificates])
            ).forEach((certificate: V1Certificate): void => { newCertificates.add(certificate); });
        }

        this.consolidateMaxHalfFullCertificates(siteConfig, currentCertificatesSorted);

        this.updateSiteCertificateMapWithCertificates(siteConfig, currentCertificatesSorted, newCertificates);

        return this.persistK8sCertificates(currentCertificatesSorted, newCertificates)
            .then((certificates: V1Certificate[]): TLSCertificateForSite[] =>
                this.mapK8sCertificatesToTLSCertificates(certificates)
            );
    }

    public async onSiteRemoved(siteConfig: SiteConfig): Promise<void> {
        return promiseAllResolved<void>(
                [...(this.siteCertificatesMap.get(siteConfig.id) ?? []).values()].map(
                    async (certificate: V1Certificate): Promise<void> =>
                        this.deleteK8sCertificate(certificate)
                            .then((): void => {
                                this.logger.info(`Removed certificate "${certificate.metadata.name}"`);
                            })
                )
            )
            .then((): void => {
                this.logger.info(`All certificates for site ${siteConfig.name} were removed.`);
            });
    }

    private cleanupDuplicatedHostNames(certificates: Set<V1Certificate>): void {
        const allHostNames: Set<string> = new Set<string>();

        certificates.forEach((certificate: V1Certificate): void => {
            const duplicatedHostNames: Set<string> = new Set<string>(certificate.spec.dnsNames.filter(
                    (hostName: string): boolean => allHostNames.has(hostName.toLowerCase())
                ));

            if (duplicatedHostNames.size > 0) {
                this.logger.debug(`Removing duplicated host names from certificate`
                    + ` ${certificate.metadata.name}: ${[...duplicatedHostNames].join(', ')}`);

                certificate.spec.dnsNames = certificate.spec.dnsNames.filter(
                    (hostName: string): boolean => !allHostNames.has(hostName.toLowerCase())
                );
            }

            certificate.spec.dnsNames.forEach(
                (dnsName: string): void => { allHostNames.add(dnsName.toLowerCase()); }
            );
        });
    }

    private mapK8sCertificatesToTLSCertificates(certificates: V1Certificate[]): TLSCertificateForSite[] {
        return certificates.map(
            (certificate: V1Certificate): TLSCertificateForSite => {
                return {
                    tlsCertificate: {
                        publicKeyFilename: this.getPublicKeyFileNameForSecretName(certificate.spec.secretName),
                        privateKeyFilename: this.getPrivateKeyFileNameForSecret(certificate.spec.secretName),
                    },
                    hostNames: certificate.spec.dnsNames,
                };
            }
        )
    }

    private updateSiteCertificateMapWithCertificates(
        siteConfig: SiteConfig,
        existingCertificates: Set<V1Certificate>,
        newCertificates: Set<V1Certificate>,
    ): void {
        this.siteCertificatesMap.set(
            siteConfig.id,
            new Map(
                [
                    ...[...existingCertificates]
                        .filter((certificate: V1Certificate): boolean => certificate.spec.dnsNames.length > 0),
                    ...newCertificates
                ]
                    .map(
                        (certificate: V1Certificate): [CertificateName, V1Certificate] =>
                            [certificate.metadata.name, certificate]
                    )
            )
        );
    }

    private fillCertificateSetWithHostNames(certificateSet: Set<V1Certificate>, hostNamesToAdd: string[]): string[] {
        //...to existing certificates...
        certificateSet.forEach(
            (certificate: V1Certificate): void => {
                const availableHostNameSlotsCount: number =
                        K8sCertificateManager.MAXIMUM_HOSTNAMES_PER_CERTIFICATE - certificate.spec.dnsNames.length,
                    hostNamesToAddToCert: string[] =
                        hostNamesToAdd.slice(0, availableHostNameSlotsCount);

                if (availableHostNameSlotsCount <= 0) {
                    return;
                }

                this.logger.info(
                    `Adding host names to certificate ${certificate.metadata.name}:`
                    + ` ${hostNamesToAddToCert.join(', ')}`
                );

                certificate.spec.dnsNames = [...certificate.spec.dnsNames, ...hostNamesToAddToCert];
                hostNamesToAdd = hostNamesToAdd.slice(availableHostNameSlotsCount);
            }
        );

        return hostNamesToAdd;
    }

    private isCertificateIssuerKind(value: string): value is V1CertificateIssuerKind {
        return ['issuer', 'clusterissuer'].includes(value.toLowerCase());
    }

    private createNewCertificatesForHostNames(siteId: number, hostNames: string[]): V1Certificate[] {
        const k8sCertIssuerRaw: string[] = this.cliArgs["k8s-certificate-issuer"].split(':', 2),
            certificateSet: V1Certificate[] = [];

        if (!this.isCertificateIssuerKind(k8sCertIssuerRaw[0]) || k8sCertIssuerRaw[1] === undefined) {
            throw new Error('Invalid value for the k8s-certificate-issuer parameter.');
        }

        const k8sCertIssuer: V1CertificateIssuerRef = {
            kind: k8sCertIssuerRaw[0],
            name: k8sCertIssuerRaw[1],
        }

        // ...and create new certificates for new host names
        while (hostNames.length > 0) {
            const hostNamesToAddToCert: string[] =
                    hostNames.slice(0, K8sCertificateManager.MAXIMUM_HOSTNAMES_PER_CERTIFICATE),
                certificateName: string = `site-certificate-${siteId}-${crypto.randomUUID()}`;

            this.logger.info(`Creating new certificate ${certificateName} with host names:`
                + ` ${hostNamesToAddToCert.join(', ')}`);

            certificateSet.push(this.createNewV1Certificate(
                siteId,
                certificateName,
                hostNames.slice(0, K8sCertificateManager.MAXIMUM_HOSTNAMES_PER_CERTIFICATE),
                k8sCertIssuer,
            ));

            hostNames = hostNames.slice(K8sCertificateManager.MAXIMUM_HOSTNAMES_PER_CERTIFICATE);
        }

        return certificateSet;
    }

    private createNewV1Certificate(
        siteId: number,
        certificateName: string,
        hostNames: string[],
        certIssuerRef: V1CertificateIssuerRef
    ): V1Certificate {
        return {
            apiVersion: 'cert-manager.io/v1',
            kind: 'Certificate',
            metadata: {
                name: certificateName,
                namespace: this.targetNamespace,
                labels: {
                    [K8sCertificateManager.SITE_ID_LABEL_KEY]: String(siteId),
                    ...K8sCertificateManager.ADDITIONAL_CERTIFICATE_LABELS
                },
            },
            spec: {
                dnsNames: hostNames,
                secretName: certificateName,
                issuerRef: certIssuerRef,
            },
        };
    }

    private removeHostNamesFromCertificates(certificates: Set<V1Certificate>, hostNames: Set<string>): void {
        certificates.forEach(
            (certificate: V1Certificate): void => {
                certificate.spec.dnsNames = certificate.spec.dnsNames.filter(
                    (dnsName: string): boolean => !hostNames.has(dnsName)
                );
            }
        );
    }

    private consolidateMaxHalfFullCertificates(siteConfig: SiteConfig, certificates: Set<V1Certificate>): void {
        certificates
            .forEach((certificate: V1Certificate): void => {
                if (certificate.spec.dnsNames.length === 0 || certificate.spec.dnsNames.includes(siteConfig.name)) {
                    return;
                }

                const certificateCandidatesToMergeWith: V1Certificate[] =
                    [...certificates]
                        .filter((certificateToTest: V1Certificate): boolean =>
                            certificateToTest !== certificate
                                && certificateToTest.spec.dnsNames.length + certificate.spec.dnsNames.length <=
                                    K8sCertificateManager.MAXIMUM_HOSTNAMES_PER_CERTIFICATE
                        )
                        .sort((certificateA: V1Certificate, certificateB: V1Certificate): number => {
                            if (certificateA.spec.dnsNames.includes(siteConfig.name)) {
                                return -1;
                            } else if (certificateB.spec.dnsNames.includes(siteConfig.name)) {
                                return 1;
                            } else {
                                return certificateB.spec.dnsNames.length - certificateA.spec.dnsNames.length;
                            }
                        });

                if (certificateCandidatesToMergeWith.length > 0) {
                    const certificateToMergeWith: V1Certificate = certificateCandidatesToMergeWith[0];

                    this.logger.info(`Consolidating certificate ${certificate.metadata.name}`
                        + ` host names to certificate ${certificateToMergeWith.metadata.name}:`
                        + ` ${certificate.spec.dnsNames.join(', ')}`
                    );

                    certificateToMergeWith.spec.dnsNames.push(...certificate.spec.dnsNames);
                    certificate.spec.dnsNames = [];
                }
            });
    }

    private async persistK8sCertificates(
        existingCertificates: Set<V1Certificate>,
        newCertificates: Set<V1Certificate>
    ): Promise<V1Certificate[]> {
        // and persist updates to existing certificates
        const deletePromises: Promise<void>[] = [],
            createOrUpdatePromises: Promise<V1Certificate>[] = [];

        // delete all certificates without any host names...
        deletePromises.push(
            ...[...existingCertificates]
                .filter((certificate: V1Certificate): boolean =>
                    certificate.spec.dnsNames.length === 0)
                .map(async (certificate: V1Certificate): Promise<void> => {
                    this.logger.info(`Removing empty certificate ${certificate.metadata.name}`);

                    existingCertificates.delete(certificate);

                    return this.deleteK8sCertificate(certificate);
                })
        );

        // ...patch other existing certificates...
        createOrUpdatePromises.push(
            ...[...existingCertificates].map(
                async (certificate: V1Certificate): Promise<V1Certificate> => {
                    // we are the authoritative source for the state of the certificate
                    // so to avoid collisions when someone updated the certificate on the server
                    // we'll ignore server updates by patching unversioned certificate
                    const newCertificate: V1Certificate = this.createNewV1Certificate(
                        parseInt(certificate.metadata.labels[K8sCertificateManager.SITE_ID_LABEL_KEY]),
                        certificate.metadata.name,
                        certificate.spec.dnsNames,
                        certificate.spec.issuerRef,
                    );

                    this.logger.debug(`Patching K8s certificate ${certificate.metadata.name}`);
                    return this.k8sObjectApi.patch<V1Certificate>(
                        newCertificate,
                        undefined,
                        undefined,
                        undefined,
                        undefined,
                        // https://github.com/kubernetes/kubernetes/issues/97423
                        PatchStrategy.MergePatch
                    )
                        .then((): V1Certificate => certificate);
                }
            )
        );

        // ...and create new certificates.
        createOrUpdatePromises.push(
            ...[...newCertificates].map(
                async (certificate: V1Certificate): Promise<V1Certificate> => {
                    this.logger.debug(`Creating K8s certificate ${certificate.metadata.name}`);
                    return this.k8sObjectApi.create<V1Certificate>(certificate).then((): V1Certificate => certificate);
                }
            )
        );

        return promiseAllResolved<void>(deletePromises)
            .then(async (): Promise<V1Certificate[]> => promiseAllResolved<V1Certificate>(createOrUpdatePromises));
    }

    private async deleteK8sCertificate(certificate: V1Certificate): Promise<void> {
        const secretSpec: KubernetesObject = {
            kind: 'Secret',
            metadata: {
                name: certificate.spec.secretName,
                namespace: certificate.metadata.namespace,
            }
        };

        return this.k8sObjectApi.delete(certificate)
            .then(async (): Promise<any> => this.k8sObjectApi.delete(secretSpec))
            .then((): void => {});
    }

    private detectCurrentNamespace(): string {
        const currentNamespace: string | undefined = this.kc.getContexts().find(
            (value: Context): boolean => value.name === this.kc.getCurrentContext()
        )?.namespace;

        if (currentNamespace === undefined) {
            throw 'Could not determine the current context/namespace.';
        }

        this.logger.debug(`Current K8s namespace: ${currentNamespace}`);

        return currentNamespace;
    }

    private async loadCertificates(): Promise<string> {
        const data: {body: object, response: IncomingMessage} =
            await this.k8sCustomObjectsApi.listNamespacedCustomObject(
                {
                    group: 'cert-manager.io',
                    version: 'v1',
                    namespace: this.targetNamespace,
                    plural: 'certificates',
                    labelSelector: K8sCertificateManager.SITE_ID_LABEL_KEY
                }
            ),
            body: V1CertificateList = data.body as V1CertificateList,
            listResourceVersion: string = body.metadata.resourceVersion!;

        this.logger.debug(`Got certificate list, resource version ${listResourceVersion}`);

        body.items.forEach(
            (certificate: V1Certificate): void => this.addCertificateToMap(certificate)
        );

        return listResourceVersion;
    }

    private addCertificateToMap(certificate: V1Certificate): void {
        const siteId: number = parseInt(certificate.metadata.labels[K8sCertificateManager.SITE_ID_LABEL_KEY]);

        this.logger.debug(`Loaded certificate ${certificate.metadata.name},`
            + ` site ID ${siteId},`
            + ` secret name ${certificate.spec.secretName},`
            + ` resource version ${certificate.metadata.resourceVersion}`
            + ` and DNS names [${certificate.spec.dnsNames.join(', ')}].`);

        if (!this.siteCertificatesMap.has(siteId)) {
            this.siteCertificatesMap.set(siteId, new Map<CertificateName, V1Certificate>());
        }

        this.siteCertificatesMap.get(siteId)!.set(certificate.metadata.name, certificate);
    }

    private removeCertificateFromTheMap(certificate: V1Certificate): void {
        const siteId: number = parseInt(certificate.metadata.labels[K8sCertificateManager.SITE_ID_LABEL_KEY]);

        this.logger.debug(`Removing certificate ${certificate.metadata.name},`
            + ` site ID ${siteId},`
            + ` secret name ${certificate.spec.secretName},`
            + ` resource version ${certificate.metadata.resourceVersion}`
            + ` and DNS names [${certificate.spec.dnsNames.join(', ')}].`);

        if (this.siteCertificatesMap.has(siteId)) {
            const certificateMap: CertificateMap = this.siteCertificatesMap.get(siteId)!;

            certificateMap.delete(certificate.metadata.name);

            if (certificateMap.size === 0) {
                this.siteCertificatesMap.delete(siteId);
            }
        }
    }

    private async loadSecretsForCertificates(resourceVersion: string): Promise<string> {
        const secretToCertificateMap: Map<string, CertificateName> = new Map<string, CertificateName>();

        this.siteCertificatesMap.forEach((certificateMap: CertificateMap): void => {
            certificateMap.forEach((certificate: V1Certificate): void => {
                secretToCertificateMap.set(certificate.spec.secretName, certificate.metadata.name);
            })
        });

        const data: V1SecretList =
            await this.k8sCoreApi.listNamespacedSecret(
                {
                    namespace: this.targetNamespace,
                    allowWatchBookmarks: false,
                    fieldSelector: K8sCertificateManager.TLS_SECRET_FIELD_SELECTOR,
                    resourceVersion: resourceVersion,
                    resourceVersionMatch: 'Exact',
                }
            ),
            listResourceVersion: string = data.metadata!.resourceVersion!;

        this.logger.debug(`Got secret list, resource version ${listResourceVersion}`);

        const fullyLoadedTLSSecrets: FullyLoadedTLSSecret[] = data.items
            .reduce<FullyLoadedTLSSecret[]>(
                (previousValue: FullyLoadedTLSSecret[], secret: V1Secret): FullyLoadedTLSSecret[] => {
                    if (this.isFullyLoadedTLSSecret(secret)) {
                        previousValue.push(secret);
                    }

                    return previousValue;
                },
                new Array<FullyLoadedTLSSecret>(),
            );

        const knownSecrets: FullyLoadedTLSSecret[] = fullyLoadedTLSSecrets.filter(
            (secret: FullyLoadedTLSSecret): boolean => {
                if (!secretToCertificateMap.has(secret.metadata.name)) {
                    this.logger.debug(`Unknown secret name ${secret.metadata.name} loaded.`);
                    return false;
                }

                this.logger.debug(
                    `Loaded secret for certificate ${secretToCertificateMap.get(secret.metadata.name)},`
                    + ` resource version ${secret.metadata.resourceVersion}`
                );
                return true;
            }
        );

        this.writeSecretsAndRemoveRedundantFiles(...knownSecrets);

        return listResourceVersion;
    }

    private isFullyLoadedTLSSecret(secret: V1Secret): secret is FullyLoadedTLSSecret {
        return (secret.metadata !== undefined)
            && (secret.metadata.name !== undefined)
            && (secret.metadata.resourceVersion !== undefined)
            && (secret.data !== undefined)
            && (secret.data['tls.crt'] !== undefined)
            && (secret.data['tls.key'] !== undefined);
    }

    private writeSecrets(...secrets: FullyLoadedTLSSecret[]): FSFilenameList[] {
        const certFileList: FSFileList = new Map<FSFilename, FSFileContents>(),
            keyFileList: FSFileList = new Map<FSFilename, FSFileContents>();

        secrets.forEach((secret: FullyLoadedTLSSecret): void => {
            certFileList.set(
                this.getPublicKeyFileNameForSecretName(secret.metadata.name),
                this.getPublicKeyContentsForSecretData(secret.data)
            );

            keyFileList.set(
                this.getPrivateKeyFileNameForSecret(secret.metadata.name),
                this.getPrivateKeyContentsForSecretData(secret.data)
            );
        });

        this.fsHelper.ensureDirSync(this.cliArgs['ssl-certs-dir'], Settings.CONFIG_DIRS_PERMISSIONS);

        this.logger.reportDirChangedInformation(
            this.fsHelper.syncFilesToDirSync(
                this.cliArgs['ssl-certs-dir'],
                certFileList,
                Settings.CONFIG_FILES_PERMISSIONS,
                false
            )
        );

        this.logger.reportDirChangedInformation(
            this.fsHelper.syncFilesToDirSync(
                this.cliArgs['ssl-certs-dir'],
                keyFileList,
                Settings.CONFIG_FILES_PERMISSIONS_PRIVATE,
                false
            )
        );

        return [new Set<FSFilename>(certFileList.keys()), new Set<FSFilename>(keyFileList.keys())];
    }

    private writeSecretsAndRemoveRedundantFiles(...secrets: FullyLoadedTLSSecret[]): void {
        const fileNameLists: FSFilenameList[] =  this.writeSecrets(...secrets),
            filesToKeep: FSFilenameList = new Set<FSFilename>(
                fileNameLists.reduce<FSFilename[]>(
                    (previousValue: FSFilename[], currentValue: FSFilenameList): FSFilename[] => {
                        return [...previousValue, ...currentValue];
                    },
                    new Array<FSFilename>()
                )
            ),
            removedFiles: FSFilenameList = this.fsHelper.emptyDirExceptFilesSync(
                this.cliArgs['ssl-certs-dir'],
                filesToKeep
            );

        this.logger.reportRemovedFiles(this.cliArgs['ssl-certs-dir'], ...removedFiles);
    }

    private getPublicKeyFileNameForSecretName(secretName: string): string {
        return `${secretName}.pem`;
    }

    private getPublicKeyContentsForSecretData(secretData: TLSSecretData): string {
        return Buffer.from(secretData['tls.crt'], 'base64').toString().trim()
            + (secretData['ca.crt']
                ? "\n" + Buffer.from(secretData['ca.crt'], 'base64').toString().trim()
                : ''
            );
    }

    private getPrivateKeyFileNameForSecret(secretName: string): string {
        return `${secretName}.key`;
    }

    private getPrivateKeyContentsForSecretData(secretData: TLSSecretData): string {
        return Buffer.from(secretData['tls.key'], 'base64').toString().trim();
    }

    private async setWatchForCertificates(resourceVersion: string): Promise<InfiniteK8sWatch> {
        const watch: InfiniteK8sWatch = this.infiniteK8sWatchFactory.build();

        watch
            .on('added', (apiObj: any) => this.onCertificateAdded(apiObj as V1Certificate))
            .on('modified', (apiObj: any) => this.onCertificateModified(apiObj as V1Certificate))
            .on('deleted', (apiObj: any) => this.onCertificateDeleted(apiObj as V1Certificate));

        this.watches.push(watch);

        return watch.start(
            `/apis/cert-manager.io/v1/namespaces/${this.targetNamespace}/certificates`,
            resourceVersion,
            {labelSelector: K8sCertificateManager.SITE_ID_LABEL_KEY},
        )
            .then((): InfiniteK8sWatch => watch);
    }

    private onCertificateAdded(certificate: V1Certificate): void {
        const certificateMetadata: V1ObjectMeta = certificate.metadata;

        this.logger.debug(`Added certificate with name ${certificateMetadata.name}`
            + ` and resource version ${certificateMetadata.resourceVersion}`);

        this.addCertificateToMap(certificate);
    }

    private onCertificateModified(certificate: V1Certificate): void {
        const certificateMetadata: V1ObjectMeta = certificate.metadata;

        this.logger.debug(`Modified certificate with name ${certificateMetadata.name}`
            + ` and resource version ${certificateMetadata.resourceVersion}`);

        this.addCertificateToMap(certificate);
    }

    private onCertificateDeleted(certificate: V1Certificate): void {
        const certificateMetadata: V1ObjectMeta = certificate.metadata;

        this.logger.debug(`Deleted certificate with name ${certificateMetadata.name}`
            + ` and resource version ${certificateMetadata.resourceVersion}`);

        this.removeCertificateFromTheMap(certificate);
    }

    private async setWatchForSecrets(resourceVersion: string): Promise<InfiniteK8sWatch> {
        const watch: InfiniteK8sWatch = this.infiniteK8sWatchFactory.build();

        watch
            .on('added', (apiObj: any) => this.onSecretAdded(apiObj as V1Secret))
            .on('modified', (apiObj: any) => this.onSecretModified(apiObj as V1Secret))
            .on('deleted', (apiObj: any) => this.onSecretDeleted(apiObj as V1Secret));

        this.watches.push(watch);
        await watch.start(
            `/api/v1/namespaces/${this.targetNamespace}/secrets`,
            resourceVersion,
            {fieldSelector: K8sCertificateManager.TLS_SECRET_FIELD_SELECTOR},
        );
        return watch;
    }

    private onSecretAdded(secret: V1Secret): void {
        if (!this.isFullyLoadedTLSSecret(secret)) {
            this.logger.debug(`Ignoring added but not a fully loaded TLS secret: ${secret.metadata?.name}`);
            return;
        }

        this.logger.debug(`Added secret with name ${secret.metadata.name}`
            + ` and resource version ${secret.metadata.resourceVersion}`);

        if (this.getLoadedCertificateForSecretName(secret.metadata.name) === undefined) {
            this.logger.warn(`Cannot find certificate for secret ${secret.metadata.name}`)
        } else {
            this.writeSecrets(secret);
        }
    }

    private onSecretModified(secret: V1Secret): void {
        if (!this.isFullyLoadedTLSSecret(secret)) {
            this.logger.debug(`Ignoring modified but not a fully loaded TLS secret: ${secret.metadata?.name}`);
            return;
        }

        this.logger.debug(`Modified secret with name ${secret.metadata.name}`
            + ` and resource version ${secret.metadata.resourceVersion}`);

        if (this.getLoadedCertificateForSecretName(secret.metadata.name) === undefined) {
            this.logger.warn(`Cannot find certificate for secret ${secret.metadata.name}`)
        } else {
            this.writeSecrets(secret);
        }
    }

    private onSecretDeleted(secret: V1Secret): void {
        if (!this.isFullyLoadedTLSSecret(secret)) {
            this.logger.debug(`Ignoring deleted but not a fully loaded TLS secret: ${secret.metadata?.name}`);
            return;
        }

        this.logger.debug(`Deleted secret with name ${secret.metadata.name}`
            + ` and resource version ${secret.metadata.resourceVersion}`);

        const removedFiles: FSFilenameList = this.fsHelper.removeSync(
            this.cliArgs['ssl-certs-dir'],
            this.getPublicKeyFileNameForSecretName(secret.metadata.name),
            this.getPrivateKeyFileNameForSecret(secret.metadata.name),
        );

        this.logger.reportRemovedFiles(this.cliArgs['ssl-certs-dir'], ...removedFiles);
    }

    private getLoadedCertificateForSecretName(secretName: string): V1Certificate | undefined {
        let foundCertificate: V1Certificate | undefined = undefined;

        this.siteCertificatesMap.forEach(
            (certificateMap: CertificateMap): void =>
                certificateMap.forEach(
                    (certificate: V1Certificate): void => {
                        if (certificate.spec.secretName === secretName) {
                            foundCertificate = certificate;
                        }
                    }
                )
        );

        return foundCertificate;
    }
}
