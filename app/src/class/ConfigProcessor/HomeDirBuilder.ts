import {SiteConfig} from "../../schema/configSchema";
import {ArgvType} from "../../schema/argvSchema";
import {Logger} from "../Logger";
import {inject, singleton} from "tsyringe";
import {promiseAllResolved} from "../../function/promiseAllResolved";
import {ExceptionWrapper} from "../ExceptionWrapper";
import {ConfigProcessor} from "./ConfigProcessor";
import {Dir, HomeDirHelper, PathType} from "../Helper/HomeDirHelper";
import {FSFilename, FSHelper, FSPermissions} from "../Helper/FSHelper";
import {ExecHelper} from "../Helper/ExecHelper";
import {Settings} from "../../Settings";
import {CancellablePromise} from "real-cancellable-promise";

@singleton()
export class HomeDirBuilder extends ConfigProcessor {
    public constructor(
        private readonly homeDirHelper: HomeDirHelper,
        private readonly logger: Logger,
        private readonly fsHelper: FSHelper,
        private readonly execHelper: ExecHelper,
        @inject('cli-args') private readonly cliArgs: ArgvType,
    ) {
        super();
    }

    public async runFull(sitesConfig: SiteConfig[]): Promise<void> {
        this.logger.info('HomeDirBuilder: full run started...');

        return promiseAllResolved<void>(
            sitesConfig.map(
                (siteConfig: SiteConfig): Promise<void> =>
                    this.build(siteConfig)
                        .then(async (): Promise<void> => this.setQuotaForSite(siteConfig))

            )
        )
        .then((): void => {
            this.logger.info('HomeDirBuilder: full run finished.');
        });
    }

    public async onSiteAdded(siteConfig: SiteConfig): Promise<void> {
        return this.build(siteConfig)
            .then(async (): Promise<void> => this.setQuotaForSite(siteConfig));
    }

    public async onSiteUpdated(oldSiteConfig: SiteConfig, newSiteConfig: SiteConfig): Promise<void> {
        return Promise.resolve()
            .then(async (): Promise<void> => {
                const oldHomeDir: FSFilename
                        = this.homeDirHelper.getPath(oldSiteConfig, PathType.MOUNTED, Dir.ROOT),
                    newHomeDir: FSFilename
                        = this.homeDirHelper.getPath(newSiteConfig, PathType.MOUNTED, Dir.ROOT);

                if (oldHomeDir === newHomeDir) {
                    return;
                }

                this.logger.info(`Scheduling renaming of homedir of old site ${oldSiteConfig.name}`
                    + ` to new site ${newSiteConfig.name} to launch in ${Settings.GRACE_TIME_FOR_SERVERS_RELOAD} secs`);

                await CancellablePromise.delay(1000 * Settings.GRACE_TIME_FOR_SERVERS_RELOAD);
                await this.fsHelper.rename(oldHomeDir, newHomeDir);
                this.logger.info(`Homedir renamed from ${oldHomeDir} to ${newHomeDir}`);
            })
            .then(async (): Promise<void> => {
                if (oldSiteConfig.quotaInGB !== newSiteConfig.quotaInGB) {
                    return this.setQuotaForSite(newSiteConfig);
                }
            });
    }

    public async onSiteRemoved(siteConfig: SiteConfig): Promise<void> {
        this.logger.info(`Scheduling archivation of homedir of site ${siteConfig.name}`
            + ` to launch in ${Settings.GRACE_TIME_FOR_SERVERS_RELOAD} secs...`);

        const homeDir: FSFilename
                = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED, Dir.ROOT),
            archivedHomeDir: FSFilename
                = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED_ARCHIVED, Dir.ROOT);

        await CancellablePromise.delay(1000 * Settings.GRACE_TIME_FOR_SERVERS_RELOAD);
        await this.fsHelper.rename(homeDir, archivedHomeDir);
        this.logger.info(`Homedir ${homeDir} of site ${siteConfig.name} archived to ${archivedHomeDir}`);
    }

    private async build(siteConfig: SiteConfig): Promise<void> {
        const siteRootDir: string = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED, Dir.ROOT),
            homeDir: string = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED, Dir.HOME),
            etcDir: string = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED, Dir.ETC),
            htdocsDir: string = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED, Dir.HTDOCS),
            logsDir: string = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED, Dir.LOGS),
            logsDirApache: string = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED, Dir.LOGS_APACHE),
            logsDirCron: string = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED, Dir.LOGS_CRON),
            logsDirPHP: string = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED, Dir.LOGS_PHP),
            tmpDir: string = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED, Dir.TMP),
            tmpPhpDir: string = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED, Dir.TMP_PHP),
            tmpUploadDir: string = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED, Dir.TMP_UPLOAD),
            apacheGid: number = this.cliArgs["apache-gid"];

        // the root dir must be root-owned and with mode 0750, in order to allow SFTP to chroot to it
        return this.fsHelper.ensureDir(siteRootDir, {uid: 0, gid: siteConfig.id, mode: 0o0750} as FSPermissions)
            .then(async (): Promise<void[]> => promiseAllResolved<void>([
                // allow Apache search permission
                this.execHelper.execute(`setfacl -m "g:${apacheGid}:x" "${siteRootDir}"`).then((): void => {}),
                this.fsHelper.ensureDir(
                    etcDir,
                    {uid: siteConfig.id, gid: siteConfig.id, mode: 0o0750} as FSPermissions
                ),
                this.fsHelper.ensureDir(
                    homeDir,
                    {uid: siteConfig.id, gid: siteConfig.id, mode: 0o0750} as FSPermissions
                ),
                this.fsHelper.ensureDir(
                    htdocsDir,
                    {uid: siteConfig.id, gid: siteConfig.id, mode: 0o0750} as FSPermissions
                )
                    .then(async (): Promise<void> => promiseAllResolved<void>([
                        // add default permissions "rX" to the Apache group
                        this.execHelper.execute(`setfacl -d -m "g:${apacheGid}:rX" "${htdocsDir}"`)
                            .then((): void => {}),
                        // add permissions "rX" to the Apache group
                        this.execHelper.execute(`setfacl -m "g:${apacheGid}:rX" "${htdocsDir}"`).then((): void => {}),
                    ]).then((): void => {})),
                this.fsHelper.ensureDir(logsDir, {uid: 0, gid: siteConfig.id, mode: 0o0750} as FSPermissions)
                    .then(async (): Promise<void> => promiseAllResolved<void>([
                        this.fsHelper.ensureDir(
                            logsDirApache,
                            {uid: 0, gid: siteConfig.id, mode: 0o2750} as FSPermissions
                        ),
                        this.fsHelper.ensureDir(
                            logsDirCron,
                            {uid: siteConfig.id, gid: siteConfig.id, mode: 0o2750} as FSPermissions
                        ),
                        this.fsHelper.ensureDir(
                            logsDirPHP,
                            {uid: siteConfig.id, gid: siteConfig.id, mode: 0o0750} as FSPermissions
                        ),
                    ]).then((): void => {})),
                this.fsHelper.ensureDir(tmpDir, {uid: 0, gid: siteConfig.id, mode: 0o0750} as FSPermissions)
                    .then(async (): Promise<void> => promiseAllResolved<void>([
                        this.fsHelper.ensureDir(
                            tmpPhpDir,
                            {uid: siteConfig.id, gid: siteConfig.id, mode: 0o0750} as FSPermissions
                        ),
                        this.fsHelper.ensureDir(
                            tmpUploadDir,
                            {uid: siteConfig.id, gid: siteConfig.id, mode: 0o0750} as FSPermissions
                        )
                            .then(async (): Promise<void> =>
                                // add default permissions "rX" to the Apache group
                                this.execHelper.execute(`setfacl -d -m "g:${apacheGid}:rX" "${tmpUploadDir}"`)
                                    .then((): void => {})
                            )
                    ]).then((): void => {})),
            ]))
            .then((): void => {
                this.logger.info(`Homedir ${siteRootDir} for site ${siteConfig.name} ensured.`)
            })
            .catch((e): never => {
                throw new ExceptionWrapper(
                    `Homedir ${siteRootDir} for site ${siteConfig.name} was not created:`,
                    e
                );
            });
    }

    private async setQuotaForSite(siteConfig: SiteConfig): Promise<void> {
        const mountPoint: string = this.cliArgs["mounted-sites-root-dir"].replace(/^(.+)\/+$/, '$1'),
            homeDir: string = this.homeDirHelper.getPath(siteConfig, PathType.MOUNTED, Dir.ROOT);

        return this.execHelper.execute(
                `xfs_quota -xc 'project -s -p ${homeDir} ${siteConfig.id}' -- ${mountPoint}`
            )
            .then(async (): Promise<string> => this.execHelper.execute(
                `xfs_quota -xc 'limit -p bhard=${siteConfig.quotaInGB}G ${siteConfig.id}' -- ${mountPoint}`
            ))
            .then((): void => {
                this.logger.info(
                    `Quota of ${siteConfig.quotaInGB}G set to the dir ${homeDir} of site ${siteConfig.name}`
                )
            })
            .catch((e): never => {
                throw new ExceptionWrapper(
                    `Error setting quota for site ${siteConfig.name} on mount point ${mountPoint}:`,
                    e
                );
            });
    }
}

