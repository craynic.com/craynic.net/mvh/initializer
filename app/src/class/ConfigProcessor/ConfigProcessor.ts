import {EventEmitter} from "events";
import {SiteConfig} from "../../schema/configSchema";

export abstract class ConfigProcessor extends EventEmitter {
    public abstract runFull(sitesConfig: SiteConfig[]): Promise<void>;

    public abstract onSiteAdded(siteConfig: SiteConfig): Promise<void>;

    public abstract onSiteUpdated(oldSiteConfig: SiteConfig, newSiteConfig: SiteConfig): Promise<void>;

    public abstract onSiteRemoved(siteConfig: SiteConfig): Promise<void>;
}
