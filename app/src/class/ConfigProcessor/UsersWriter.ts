import {ArgvType} from "../../schema/argvSchema";
import {SiteConfig} from "../../schema/configSchema";
import {inject, singleton} from "tsyringe";
import {Logger} from "../Logger";
import {Settings} from "../../Settings";
import {ConfigProcessor} from "./ConfigProcessor";
import {Dir, HomeDirHelper, PathType} from "../Helper/HomeDirHelper";
import {
    FSFileContents,
    FSFileList,
    FSFilename,
    FSHelper
} from "../Helper/FSHelper";

@singleton()
export class UsersWriter extends ConfigProcessor {
    private cachedSiteConfigs: SiteConfig[] = [];

    public constructor(
        private readonly logger: Logger,
        private readonly homeDirHelper: HomeDirHelper,
        private readonly fsHelper: FSHelper,
        @inject('cli-args') private readonly cliArgs: ArgvType,
    ) {
        super();
    }

    public async runFull(sitesConfig: SiteConfig[]): Promise<void> {
        this.logger.info('UsersWriter: full run started...');

        // make a copy of the array
        this.cachedSiteConfigs = [...sitesConfig];

        await this.onChange();
        this.logger.info('UsersWriter: full run finished.');
    }

    public async onSiteAdded(siteConfig: SiteConfig): Promise<void> {
        this.cachedSiteConfigs.push(siteConfig);

        await this.onChange();
    }

    public async onSiteUpdated(oldSiteConfig: SiteConfig, newSiteConfig: SiteConfig): Promise<void> {
        this.cachedSiteConfigs = this.cachedSiteConfigs.map(
            (cachedSiteConfig: SiteConfig): SiteConfig =>
                cachedSiteConfig.id === oldSiteConfig.id ? newSiteConfig : cachedSiteConfig
        );

        await this.onChange();
    }

    public async onSiteRemoved(siteConfig: SiteConfig): Promise<void> {
        this.cachedSiteConfigs = this.cachedSiteConfigs.filter(
            (cachedSiteConfig: SiteConfig): boolean => cachedSiteConfig.id !== siteConfig.id
        );

        await this.onChange();
    }

    private async onChange(): Promise<void> {
        await this.fsHelper.ensureDir(this.cliArgs["users-dir"], Settings.CONFIG_DIRS_PERMISSIONS);

        const filelist: FSFileList = new Map<FSFilename, FSFileContents>();

        filelist.set('passwd', this.generatePasswdFile(this.cachedSiteConfigs, true));
        filelist.set('passwd-nopass', this.generatePasswdFile(this.cachedSiteConfigs, false));

        this.logger.reportDirChangedInformation(
            await this.fsHelper.syncFilesToDir(
                this.cliArgs["users-dir"],
                filelist,
                Settings.CONFIG_FILES_PERMISSIONS
            )
        );
    }

    private generatePasswdFile(sitesConfig: SiteConfig[], writePasswords: boolean): FSFileContents {
        return sitesConfig.map(
            (siteConfig: SiteConfig): string => [
                siteConfig.username,
                (writePasswords ? siteConfig.sftp?.password : null) ?? 'x',
                String(siteConfig.id),
                String(siteConfig.id),
                siteConfig.name,
                this.homeDirHelper.getPath(siteConfig, PathType.TARGET, Dir.ROOT),
                '/bin/false',
            ].join(':')
        ).join("\n");
    }
}
