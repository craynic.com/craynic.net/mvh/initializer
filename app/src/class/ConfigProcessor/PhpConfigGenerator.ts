import {inject, singleton} from "tsyringe";
import {PhpVersions, PhpVersion, SiteConfig} from "../../schema/configSchema";
import {ArgvType} from "../../schema/argvSchema";
import {Logger} from "../Logger";
import {promiseAllResolved} from "../../function/promiseAllResolved";
import {Settings} from "../../Settings";
import {ConfigProcessor} from "./ConfigProcessor";
import {
    FSFileContents,
    FSFileList,
    FSFilename,
    FSFilenameList,
    FSFileStruct,
    FSHelper
} from "../Helper/FSHelper";
import {PhpHelper} from "../Helper/PhpHelper";
import {ApacheConfigGenerator} from "./ApacheConfigGenerator";
import {CancellablePromise} from "real-cancellable-promise";

type PhpVersionedConfig = {phpConfig: FSFileStruct, phpVersion: PhpVersion};
type PhpVersionedConfigFilelist = Map<PhpVersion, FSFileList>;

@singleton()
export class PhpConfigGenerator extends ConfigProcessor {
    public constructor(
        private readonly logger: Logger,
        private readonly fsHelper: FSHelper,
        private readonly phpHelper: PhpHelper,
        private readonly apacheConfigGenerator: ApacheConfigGenerator,
        @inject('cli-args') private readonly cliArgs: ArgvType,
    ) {
        super();
    }

    public async runFull(sitesConfig: SiteConfig[]): Promise<void> {
        this.logger.info('PhpConfigGenerator: full run started...');

        await promiseAllResolved(
            PhpVersions.map(async (phpVersion: PhpVersion): Promise<void> => {
                await this.fsHelper.ensureDir(
                    this.phpHelper.getPoolSocketRootPathForPhpVersion(phpVersion),
                    Settings.PHP_POOLS_DIRS_PERMISSIONS
                );
            })
        );

        await this.writePhpversionedConfigFileList(
            this.getPhpVersionedConfigFileList(...sitesConfig),
            true
        );

        this.logger.info('PhpConfigGenerator: full run finished.');
    }

    public async onSiteAdded(siteConfig: SiteConfig): Promise<void> {
        await this.writePhpversionedConfigFileList(
            this.getPhpVersionedConfigFileList(siteConfig),
            false
        );
    }

    public async onSiteUpdated(oldSiteConfig: SiteConfig, newSiteConfig: SiteConfig): Promise<void> {
        await this.onSiteAdded(newSiteConfig);

        if (this.phpHelper.havePhpVersionsChanged(oldSiteConfig, newSiteConfig)) {
            this.logger.info(`Site ${oldSiteConfig.name} PHP version changed, orchestrating updates gracefully...`);

            this.logger.info(
                `Waiting ${Settings.GRACE_TIME_FOR_SERVERS_RELOAD} secs for PHP to pick up changes...`
            );
            await CancellablePromise.delay(1000 * Settings.GRACE_TIME_FOR_SERVERS_RELOAD);

            await this.apacheConfigGenerator.onSiteUpdated(oldSiteConfig, newSiteConfig);

            this.logger.info(
                `Waiting ${Settings.GRACE_TIME_FOR_SERVERS_RELOAD} secs for Apache to pick up changes...`
            );
            await CancellablePromise.delay(1000 * Settings.GRACE_TIME_FOR_SERVERS_RELOAD);
        }

        const oldBaseFileName: FSFilename = this.getBaseConfigFileName(oldSiteConfig),
            newBaseFileName: FSFilename = this.getBaseConfigFileName(newSiteConfig);

        if (oldBaseFileName !== newBaseFileName) {
            await this.removeSiteConfigForAllConfiguredPhpVersions(oldSiteConfig);
        } else {
            await this.removeSiteConfigForPhpVersions(
                oldSiteConfig,
                this.phpHelper.getRemovedPhpVersions(oldSiteConfig, newSiteConfig)
            );
        }
    }

    public async onSiteRemoved(siteConfig: SiteConfig): Promise<void> {
        await this.removeSiteConfigForAllConfiguredPhpVersions(siteConfig);
    }

    private async removeSiteConfigForAllConfiguredPhpVersions(siteConfig: SiteConfig): Promise<void> {
        await this.removeSiteConfigForPhpVersions(
            siteConfig,
            this.phpHelper.getPhpVersionsToConfigureForSite(siteConfig)
        );
    }

    private async removeSiteConfigForPhpVersions(siteConfig: SiteConfig, phpVersions: Set<PhpVersion>): Promise<void> {
        await promiseAllResolved<void>(
            [...phpVersions]
                .map((phpVersion: PhpVersion): Promise<void> =>
                    this.removeSiteConfigForPhpVersion(siteConfig, phpVersion)
                )
        );
    }

    private async removeSiteConfigForPhpVersion(siteConfig: SiteConfig, phpVersion: PhpVersion): Promise<void> {
        const phpVersionPoolDir: FSFilename = this.getPhpVersionPoolDir(phpVersion),
            baseConfigFileName: FSFilename = this.getBaseConfigFileName(siteConfig),
            removedFiles: FSFilenameList = await this.fsHelper.remove(phpVersionPoolDir, baseConfigFileName);

        this.logger.reportRemovedFiles(phpVersionPoolDir, ...removedFiles)
    }

    private async writePhpversionedConfigFileList(
        phpVersionedConfigFileList: PhpVersionedConfigFilelist,
        shouldRemoveRedundantFiles: boolean
    ): Promise<void> {
        await promiseAllResolved<void>(
            PhpVersions.map(async (phpVersion: PhpVersion): Promise<void> => {
                const versionPoolDDir: FSFilename = this.getPhpVersionPoolDir(phpVersion);

                await this.fsHelper.ensureDir(versionPoolDDir, Settings.CONFIG_DIRS_PERMISSIONS);

                this.logger.reportDirChangedInformation(
                    await this.fsHelper.syncFilesToDir(
                        versionPoolDDir,
                        phpVersionedConfigFileList.has(phpVersion)
                            ? phpVersionedConfigFileList.get(phpVersion)!
                            : new Map<FSFilename, FSFileContents>(),
                        Settings.CONFIG_FILES_PERMISSIONS,
                        shouldRemoveRedundantFiles
                    )
                );

                this.logger.reportDirChangedInformation(
                    await this.fsHelper.syncFilesToDir(
                        versionPoolDDir,
                        new Map<FSFilename, FSFileContents>([[Settings.READY_FILE_FILENAME, '']]),
                        Settings.CONFIG_FILES_PERMISSIONS,
                        false
                    )
                );
            })
        );

        if (shouldRemoveRedundantFiles) {
            const removedDirs: FSFilenameList = await this.fsHelper
                .emptyDirExceptFiles(this.cliArgs["php-pool-dir"], new Set<FSFilename>(PhpVersions));

            this.logger.reportRemovedFiles(this.cliArgs["php-pool-dir"], ...removedDirs);
        }
    }

    private getPhpVersionedConfigFileList(...siteConfigs: SiteConfig[]): PhpVersionedConfigFilelist {
        return siteConfigs
            .map<PhpVersionedConfig[]>((siteConfig: SiteConfig): PhpVersionedConfig[] =>
                this.getPhpVersionedConfigListFromSiteConfig(siteConfig))
            .reduce<PhpVersionedConfigFilelist>(
                (
                    previousValue: PhpVersionedConfigFilelist,
                    currentValue: PhpVersionedConfig[]
                ): PhpVersionedConfigFilelist => {
                    currentValue.forEach(
                        (versionedConfig: PhpVersionedConfig): void => {
                            if (!previousValue.has(versionedConfig.phpVersion)) {
                                previousValue.set(versionedConfig.phpVersion, new Map<FSFilename, FSFileContents>());
                            }

                            previousValue.get(versionedConfig.phpVersion)!.set(
                                versionedConfig.phpConfig.filename,
                                versionedConfig.phpConfig.contents
                            );
                        }
                    )

                    return previousValue;
                },
                new Map<PhpVersion, FSFileList>()
            );
    }

    private getPhpVersionedConfigListFromSiteConfig(siteConfig: SiteConfig): PhpVersionedConfig[] {
        return [...this.phpHelper.getPhpVersionsToConfigureForSite(siteConfig)]
            .map(
                (phpVersion: PhpVersion): PhpVersionedConfig => {
                    return {
                        phpConfig: {
                            filename: this.getBaseConfigFileName(siteConfig),
                            contents: this.generateConfigFile(siteConfig, phpVersion)
                        },
                        phpVersion: phpVersion,
                    };
                }
            );
    }

    private getPhpVersionPoolDir(phpVersion: PhpVersion): FSFilename {
        return `${this.cliArgs["php-pool-dir"]}/${phpVersion}`;
    }

    private getBaseConfigFileName(siteConfig: SiteConfig): FSFilename {
        return `${siteConfig.name}.conf`;
    }

    private generateConfigFile(config: SiteConfig, phpVersion: PhpVersion): string {
        const targetSitesRootDir: FSFilename = this.cliArgs["target-sites-root-dir"],
            listenConfig: string = this.phpHelper.useUnixSockets()
                ? `listen = ${this.phpHelper.getPoolSocketPath(config, phpVersion)}
listen.owner = ${config.id}
listen.group = ${this.cliArgs["apache-gid"]}`
                : `listen = ${this.phpHelper.getPoolHostPort(config, phpVersion)}`;

        return `[${config.name}]

prefix = ${targetSitesRootDir}/$pool
chdir = ${targetSitesRootDir}/$pool/htdocs
user = ${config.id}
group = ${config.id}

request_terminate_timeout = ${config.php.request_timeout}

${listenConfig}

pm = dynamic
pm.max_children = 5
pm.start_servers = 1
pm.min_spare_servers = 1
pm.max_spare_servers = 3
pm.max_requests = 200

env["HOSTNAME"] = "$pool"
env["TMP"] = "${targetSitesRootDir}/$pool/tmp/php/"
env["TMPDIR"] = "${targetSitesRootDir}/$pool/tmp/php/"
env["TEMP"] = "${targetSitesRootDir}/$pool/tmp/php/"
${Object
    .entries<string>(config.php.env ?? {})
    .map(([key, val]: [string, string]): string => `env["${key}"] = "${val}"`)
    .join("\n")
}

php_admin_flag[log_errors] = On
php_admin_value[error_log] = ${targetSitesRootDir}/$pool/logs/php/error.log
php_admin_value[session.save_path] = ${targetSitesRootDir}/$pool/tmp/php/
php_admin_value[upload_tmp_dir] = ${targetSitesRootDir}/$pool/tmp/upload/
php_admin_value[sys_temp_dir] = ${targetSitesRootDir}/$pool/tmp/php/
php_admin_value[opcache.lockfile_path] = ${targetSitesRootDir}/$pool/tmp/php/
php_admin_value[soap.wsdl_cache_dir] = ${targetSitesRootDir}/$pool/tmp/php/
php_admin_value[open_basedir] = ${targetSitesRootDir}/$pool/
php_admin_value[max_execution_time] = ${config.php.request_timeout}
php_admin_value[max_input_time] = ${config.php.request_timeout}
php_admin_value[memory_limit] = ${config.php.memory_limit}
`;
    }
}
