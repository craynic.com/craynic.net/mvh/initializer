import * as winston from "winston";
import {inject, singleton} from "tsyringe";
import {ExceptionWrapper} from "./ExceptionWrapper";
import {FSDirChangedInformation, FSFilename} from "./Helper/FSHelper";

function createLogger(level: string): winston.Logger {
    const myFormat: winston.Logform.Format = winston.format.printf(
        ({level, message, label, timestamp, details}) =>
            `${timestamp} [${label}] ${level}: ${message} ${details !== undefined ? JSON.stringify(details) : ''}`
    );

    return winston.createLogger({
        level: level,
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.label({label: 'logger'}),
            winston.format.prettyPrint(),
            winston.format.colorize(),
            myFormat
        ),
        transports: new winston.transports.Console(),
    });
}

// noinspection JSUnusedGlobalSymbols
@singleton()
export class Logger {
    private logger: winston.Logger;

    constructor(@inject('log-level') level: string) {
        this.logger = createLogger(level);
    }

    public debug(message: string, ...meta: any[]): void {
        this.logger.debug(message, ...meta);
    }

    public info(message: string, ...meta: any[]): void {
        this.logger.info(message, ...meta);
    }

    public warn(message: string, ...meta: any[]): void {
        this.logger.warn(message, ...meta);
    }

    public error(message: string, ...meta: any[]): void {
        this.logger.error(message, ...meta);
    }

    public exception(exception: any): void {
        if (exception instanceof ExceptionWrapper) {
            this.error(exception.message);
            this.exception(exception.wrappedException);
        } else if (exception instanceof Array) {
            exception.forEach((exceptionItem: any): void => this.exception(exceptionItem));
        } else if (exception instanceof Error) {
            exception.message.split("\n").forEach((line: string) => this.error(line));

            if (exception.stack) {
                this.error(exception.stack);
            }
        } else {
            this.error(JSON.stringify(exception));
        }
    }

    public reportDirChangedInformation(changedInfo: FSDirChangedInformation): void {
        changedInfo.createdFiles.forEach((configFilename: string): void => {
            this.info(`Created file ${changedInfo.absolutePathToDir}/${configFilename}`);
        });

        changedInfo.changedFiles.forEach((configFilename: string): void => {
            this.info(`Changed file ${changedInfo.absolutePathToDir}/${configFilename}`);
        });

        changedInfo.unchangedFiles.forEach((configFilename: string): void => {
            this.debug(`Unchanged file ${changedInfo.absolutePathToDir}/${configFilename}`);
        });

        this.reportRemovedFiles(changedInfo.absolutePathToDir, ...changedInfo.removedFiles);

        this.debug(`Summary for changes in directory ${changedInfo.absolutePathToDir}:`
            + ` ${changedInfo.createdFiles.size} created, ${changedInfo.changedFiles.size} changed,`
            + ` ${changedInfo.removedFiles.size} removed, ${changedInfo.unchangedFiles.size} unchanged`);
    }

    public reportRemovedFiles(absolutePathToDir: FSFilename, ...removedFiles: FSFilename[]): void {
        removedFiles.forEach((removedFile: string): void => {
            this.info(`Removed file/dir ${absolutePathToDir}/${removedFile}`);
        });
    }
}
