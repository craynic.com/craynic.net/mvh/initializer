import "reflect-metadata";
import {config} from "dotenv";
import {container} from "tsyringe";
import {Logger} from "./class/Logger";
import {Main} from "./class/Main";

// enable ENV variables
config({path: __dirname + '/../.env'});

import "./di";

const logger: Logger = container.resolve(Logger);

container.resolve(Main).run().catch((e): never => {
    logger.exception(e);
    process.exit(1);
});
