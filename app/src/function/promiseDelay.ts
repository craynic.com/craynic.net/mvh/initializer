import {CancellablePromise, Cancellation} from "real-cancellable-promise";

export function promiseDelay(delayInSecs: number): CancellablePromise<void> {
    let cancel: () => void = (): void => {};

    let promise: Promise<void> = new Promise<void>(
        (resolve: (value: void | Promise<void>) => void, reject: (reason?: any) => void): void => {
            let timeout: NodeJS.Timeout|null = setTimeout((): void => {
                timeout = null;
                resolve();
            }, 1000 * delayInSecs);

            cancel = (): void => {
                if (timeout !==  null) {
                    clearTimeout(timeout);
                }

                reject(new Cancellation('Timeout cancelled.'));
            }
        }
    );

    return new CancellablePromise(promise, cancel);
}
