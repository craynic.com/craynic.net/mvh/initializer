
export async function promiseAllResolved<T>(values: Promise<T>[]): Promise<T[]> {
    return Promise
        .allSettled<Promise<T>>(values)
        .then((values: PromiseSettledResult<T>[]): T[] => {
            const rejectedResults: PromiseRejectedResult[] = values.filter(
                (value: PromiseSettledResult<T>): boolean => value.status === 'rejected'
            ) as PromiseRejectedResult[];

            if (rejectedResults.length > 0) {
                throw rejectedResults.map((value: PromiseRejectedResult): any => value.reason);
            }

            return (values as PromiseFulfilledResult<T>[]).map((value: PromiseFulfilledResult<T>): T => value.value);
        });
}