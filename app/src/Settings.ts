import {FSPermissions} from "./class/Helper/FSHelper";

export class Settings {
    public static readonly LOGGING = {LOG_LEVEL: 'info'};
    public static readonly SFTP_KEYS_PERMISSIONS =
        {ROOT_DIR: 0o0755, DIRS: 0o0750, FILES: 0o0640};
    public static readonly CONFIG_FILES_PERMISSIONS: FSPermissions = {uid: null, gid: null, mode: 0o0640};
    public static readonly CONFIG_DIRS_PERMISSIONS: FSPermissions = {uid: null, gid: null, mode: 0o750};
    public static readonly CONFIG_FILES_PERMISSIONS_PRIVATE: FSPermissions = {uid: null, gid: null, mode: 0o0400};
    public static readonly GRACE_TIME_FOR_SERVERS_RELOAD: number = 10;
    public static readonly READY_FILE_FILENAME: string = '.ready';
    public static readonly PHP_POOLS_DIRS_PERMISSIONS: FSPermissions = {uid: 0, gid: 0, mode: 0o0755};
}
