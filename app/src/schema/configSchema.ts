export const PhpVersions = ['5.6', '7.4', '8.0', '8.1', '8.2', '8.3', '8.4'] as const;
export type PhpVersion = typeof PhpVersions[number];

interface SitePhpPmConfig {
    max_children: number,
}

interface SitePhpConfig {
    version: PhpVersion,
    request_timeout: number,
    pm: SitePhpPmConfig,
    memory_limit: string,
    env?: {
        [name: string]: string,
    },
    enable_all_versions?: boolean,
}

interface SiteSftpConfig {
    keys?: Array<string>,
    password?: string,
}

export interface SiteConfig {
    id: number,
    name: string,
    username: string,
    quotaInGB: number,
    aliases?: string[],
    php: SitePhpConfig,
    sftp?: SiteSftpConfig,
}
