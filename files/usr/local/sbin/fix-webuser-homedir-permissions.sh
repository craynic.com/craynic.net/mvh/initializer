#!/usr/bin/env bash

set -Eeuo pipefail

PROJECT_ID="$1"
PROJECT_ROOT_DIR="$2"
APACHE_GID="${APP_APACHE_GID:-33}"

usage() {
  (
    echo ""
    echo "Usage: $0 <project-id> <project-root-dir>"
  ) >/dev/stderr
}

RE='^[0-9]+$'
if ! [[ "$PROJECT_ID" =~ $RE ]]; then
  echo "Invalid project-id \"$PROJECT_ID\"!" >/dev/stderr
  usage
  exit 1
fi

if ! [[ -d "$PROJECT_ROOT_DIR" ]]; then
  echo "project-root-dir \"$PROJECT_ROOT_DIR\" does not exist." >/dev/stderr
  usage
  exit 1
elif ! [[ -d "$PROJECT_ROOT_DIR/htdocs" ]]; then
  echo "project-root-dir \"$PROJECT_ROOT_DIR\" exists, but it does not look like a root dir of a project!" >/dev/stderr
  usage
  exit 1
fi

# no group writable, no others access
chmod -R ugo-x,g-s,o-rwx,u+rwX,g+rX -- "$PROJECT_ROOT_DIR"
# remove all ACLs
setfacl -R -b -- "$PROJECT_ROOT_DIR"

# project root
chown "root:$PROJECT_ID" -- "$PROJECT_ROOT_DIR"
# add search permission for Apache
setfacl -m "g:$APACHE_GID:x" -- "$PROJECT_ROOT_DIR"

# etc dir
chown -R "$PROJECT_ID:$PROJECT_ID" -- "$PROJECT_ROOT_DIR/etc"

# home dir
chown -R "$PROJECT_ID:$PROJECT_ID" -- "$PROJECT_ROOT_DIR/home"

# htdocs dir
chown -R "$PROJECT_ID:$PROJECT_ID" -- "$PROJECT_ROOT_DIR/htdocs"
# add access for Apache
setfacl -d -R -m "g:$APACHE_GID:rX" -- "$PROJECT_ROOT_DIR/htdocs"
setfacl    -R -m "g:$APACHE_GID:rX" -- "$PROJECT_ROOT_DIR/htdocs"

# logs dir
chown "root:$PROJECT_ID" -- "$PROJECT_ROOT_DIR/logs"
# logs for Apache
chmod g+s -- "$PROJECT_ROOT_DIR/logs/apache"
chown -R "root:$PROJECT_ID" -- "$PROJECT_ROOT_DIR/logs/apache"
chown -R "$PROJECT_ID:$PROJECT_ID" -- "$PROJECT_ROOT_DIR/logs/php"

# tmp dir
chown "root:$PROJECT_ID" -- "$PROJECT_ROOT_DIR/tmp"
# tmp-php dir
chown -R "$PROJECT_ID:$PROJECT_ID" -- "$PROJECT_ROOT_DIR/tmp/php"
# tmp-upload dir
chown -R "$PROJECT_ID:$PROJECT_ID" -- "$PROJECT_ROOT_DIR/tmp/upload"
# add default ACLs: once the file will get moved to the htdocs, it needs to be accessible by Apache group
setfacl -d -R -m "g:$APACHE_GID:rX" -- "$PROJECT_ROOT_DIR/tmp/upload"
